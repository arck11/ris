import json
import os
import sys
from datetime import datetime
from pathlib import Path
from typing import List, Tuple

import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
import matplotlib.pyplot as plt


def draw(data: List[List], label: str, *, xlabel: str = "Epochs", ylabel: str = None):
    # Построение графика ошибки

    plt.clf()

    epochs = range(1, len(data) + 1)
    plt.plot(epochs, data, 'b', label=label)
    plt.title(label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()

    plt.show()


def get_model(x, y):
    model = Sequential()
    model.add(LSTM(256, input_shape=(x.shape[1], x.shape[2]), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(256))
    model.add(Dropout(0.2))
    model.add(Dense(y.shape[1], activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam')

    return model


def get_data():
    filename = "wonderland.txt"
    with open(filename) as f:
        raw_text = open(filename).read().lower()

    chars = sorted(set(raw_text))
    char_to_int = {c: i for i, c in enumerate(chars)}

    n_chars = len(raw_text)
    n_vocab = len(chars)

    print(n_chars)
    print(n_vocab)

    seq_length = 100
    data_x = []
    data_y = []
    for i in range(0, n_chars - seq_length, 1):
        seq_in = raw_text[i:i + seq_length]
        seq_out = raw_text[i + seq_length]
        data_x.append([char_to_int[char] for char in seq_in])
        data_y.append(char_to_int[seq_out])
    n_patterns = len(data_x)

    # reshape X to be [samples, time steps, features]
    X = np.reshape(data_x, (n_patterns, seq_length, 1))
    # normalize
    X = X / float(n_vocab)
    # one hot encode the output variable
    y = np_utils.to_categorical(data_y)

    return X, y, raw_text, chars, char_to_int, data_x, data_y


def train(epochs: int = 20, batch_size: int = 128):
    x, y, raw_text, chars, char_to_int, data_x, data_y = get_data()
    model = get_model(x, y)

    # define the checkpoint
    checkpoint_dir = datetime.now().isoformat().split(".")[0].replace(":", "_").replace("-", "_")

    if not os.path.exists(checkpoint_dir):
        os.mkdir(checkpoint_dir)
    filepath = os.path.join(checkpoint_dir, "weights-improvement-{epoch:02d}-{loss:.4f}.hdf5")
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    callbacks_list = [checkpoint]

    model.load_weights("2020_04_02T20_26_28/weights-improvement-05-2.2568.hdf5")
    H = model.fit(x, y, epochs=epochs, batch_size=batch_size, callbacks=callbacks_list)

    model.save("l8.h5")
    draw(H.history["loss"], "Training loss", ylabel="loss")
    # draw(H.history["accuracy"], "Training accuracy", ylabel="accuracy")
    # draw(H.history["val_loss"], "Validate loss", ylabel="loss")
    # draw(H.history["val_accuracy"], "Validate accuracy", ylabel="accuracy")


def generate(point_filename: str, generate_size: int = 1000):
    x, y, raw_text, chars, char_to_int, data_x, data_y = get_data()
    n_chars = len(raw_text)
    n_vocab = len(chars)

    model = get_model(x, y)
    # load the network weights

    model.load_weights(point_filename)
    model.compile(loss='categorical_crossentropy', optimizer='adam')

    int_to_char = dict((i, c) for i, c in enumerate(chars))

    # pick a random seed
    start = np.random.randint(0, len(data_x) - 1)
    pattern = data_x[start]
    start_pattern = ''.join([int_to_char[value] for value in pattern])
    print("Pattern:", start_pattern)
    # generate characters
    generated_text = ""
    for i in range(generate_size):
        x = np.reshape(pattern, (1, len(pattern), 1))
        x = x / float(n_vocab)
        prediction = model.predict(x, verbose=0)
        index = np.argmax(prediction)
        result = int_to_char[index]
        seq_in = [int_to_char[value] for value in pattern]
        generated_text += result
        pattern.append(index)
        pattern = pattern[1:len(pattern)]
    return start_pattern, generated_text

def main():
    filename = "wonderland.txt"
    with open(filename) as f:
        raw_text = open(filename).read().lower()

    chars = sorted(set(raw_text))
    char_to_int = {c: i for i, c in enumerate(chars)}

    n_chars = len(raw_text)
    n_vocab = len(chars)

    print(n_chars)
    print(n_vocab)

    seq_length = 100
    data_x = []
    data_y = []
    for i in range(0, n_chars - seq_length, 1):
        seq_in = raw_text[i:i + seq_length]
        seq_out = raw_text[i + seq_length]
        data_x.append([char_to_int[char] for char in seq_in])
        data_y.append(char_to_int[seq_out])
    n_patterns = len(data_x)

    # reshape X to be [samples, time steps, features]
    X = np.reshape(data_x, (n_patterns, seq_length, 1))
    # normalize
    X = X / float(n_vocab)
    # one hot encode the output variable
    y = np_utils.to_categorical(data_y)

    model = get_model(X, y)

    # define the checkpoint
    filepath = "weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
    callbacks_list = [checkpoint]

    H = model.fit(X, y, epochs=20, batch_size=128, callbacks=callbacks_list)

    model.save("l8.h5")
    draw(H.history["loss"], "Training loss", ylabel="loss")
    # draw(H.history["accuracy"], "Training accuracy", ylabel="accuracy")
    # draw(H.history["val_loss"], "Validate loss", ylabel="loss")
    # draw(H.history["val_accuracy"], "Validate accuracy", ylabel="accuracy")


if __name__ == '__main__':
    train(45, 256)
    # path = Path("2020_04_02T17_12_08") / "weights-improvement-20-1.9457.hdf5"
    #
    # s, g = generate(path.as_posix())
    #
    # print(s)
    # print("--------")
    # print(g)
    # results = []
    #
    # for file in path.iterdir():
    #     s, g = generate(file.as_posix())
    #     print(s)
    #     with file.with_suffix(".json").open("w") as f:
    #         json.dump({
    #             "pattern": s,
    #             "generated": g
    #         }, f, indent=4)
