from typing import List

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from keras.utils import to_categorical
from keras import models
from keras import layers
from keras.datasets import imdb


def get_dataset():
    (training_data, training_targets), (testing_data, testing_targets) = imdb.load_data(num_words=10000)
    data = np.concatenate((training_data, testing_data), axis=0)
    targets = np.concatenate((training_targets, testing_targets), axis=0)

    return data, targets


def print_review(review: List[int]):
    index = imdb.get_word_index()
    reverse_index = dict([(value, key) for (key, value) in index.items()])
    decoded = " ".join([reverse_index.get(i - 3, "#") for i in review])
    return decoded


def vectorize(sequences, dimension=10000):
    results = np.zeros((len(sequences), dimension))

    for i, sequence in enumerate(sequences):
        results[i, sequence] = 1
    return results


def draw_accuracy(data: List[List]):
    # Построение графика отклонения

    plt.clf()

    epochs = range(1, len(data) + 1)
    plt.plot(epochs, data, 'b', label='Traning accuracy')
    plt.title('Training accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Mae')
    plt.legend()

    plt.show()


def draw_loss(data: List[List], label: str):
    # Построение графика ошибки

    plt.clf()

    epochs = range(1, len(data) + 1)
    plt.plot(epochs, data, 'b', label='Traning loss')
    plt.title('Training loss')
    plt.xlabel('Epochs')
    plt.ylabel('loss')
    plt.legend()

    plt.show()


def draw(data: List[List], label: str, *, xlabel: str = "Epochs", ylabel: str = None):
    # Построение графика ошибки

    plt.clf()

    epochs = range(1, len(data) + 1)
    plt.plot(epochs, data, 'b', label=label)
    plt.title(label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()

    plt.show()


def build_model():
    model = models.Sequential()
    # Input - Layer
    model.add(layers.Dense(50, activation="relu", input_shape=(10000,)))
    # Hidden - Layers
    model.add(layers.Dropout(0.3, noise_shape=None, seed=None))
    model.add(layers.Dense(100, activation="relu"))
    model.add(layers.Dropout(0.2, noise_shape=None, seed=None))
    model.add(layers.Dense(150, activation="relu"))
    model.add(layers.Dropout(0.1, noise_shape=None, seed=None))
    model.add(layers.Dense(100, activation="relu"))

    # Output- Layer
    model.add(layers.Dense(1, activation="sigmoid"))
    model.summary()

    model.compile(
        optimizer="adam",
        loss="binary_crossentropy",
        metrics=["accuracy"]
    )

    return model


def train(model, train_x, train_y, test_x, test_y):
    results = model.fit(
        train_x, train_y,
        epochs=2,
        batch_size=500,
        validation_data=(test_x, test_y)
    )

    return results


def main():
    data, targets = get_dataset()
    print("Categories:", np.unique(targets))
    print("Number of unique words:", len(np.unique(np.hstack(data))))
    length = [len(i) for i in data]
    print("Average Review length:", np.mean(length))
    print("Standard Deviation:", round(np.std(length)))
    print(data[1])
    print(print_review(data[1]))
    data = vectorize(data)
    targets = np.array(targets).astype("float32")

    test_x = data[:10000]
    test_y = targets[:10000]
    train_x = data[10000:]
    train_y = targets[10000:]

    model = build_model()

    results = train(model, train_x, train_y, test_x, test_y)

    draw(results.history["loss"], "Training loss", ylabel="loss")
    draw(results.history["accuracy"], "Training accuracy", ylabel="accuracy")
    draw(results.history["val_loss"], "Validate loss", ylabel="loss")
    draw(results.history["val_accuracy"], "Validate accuracy", ylabel="accuracy")

    model.save("l6_7.h5")
    print(results)


if __name__ == '__main__':
    main()
