import os
from typing import List

import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Conv1D, MaxPooling1D, Dropout
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.datasets import imdb
import matplotlib.pyplot as plt

# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

def get_data():
    (training_data, training_targets), (testing_data, testing_targets) = imdb.load_data(num_words=10000)
    data = np.concatenate((training_data, testing_data), axis=0)
    targets = np.concatenate((training_targets, testing_targets), axis=0)

    return data, targets


def draw(data: List[List], label: str, *, xlabel: str = "Epochs", ylabel: str = None):
    # Построение графика ошибки

    plt.clf()

    epochs = range(1, len(data) + 1)
    plt.plot(epochs, data, 'b', label=label)
    plt.title(label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()

    plt.show()


def main():
    data, targets = get_data()

    # split data 80 / 20
    test_x = data[:10000]
    test_y = targets[:10000]
    train_x = data[10000:]
    train_y = targets[10000:]

    max_review_length = 500
    train_x = sequence.pad_sequences(train_x, maxlen=max_review_length)
    test_x = sequence.pad_sequences(test_x, maxlen=max_review_length)

    embedding_vecor_length = 32
    model = Sequential()
    model.add(Embedding(5000, embedding_vecor_length, input_length=max_review_length, dropout=0.2))
    model.add(Conv1D(filters=32, kernel_size=3, padding='same', activation='relu'))
    model.add(MaxPooling1D(pool_size=2))
    model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())
    H = model.fit(train_x, train_y, validation_data=(test_x, test_y), epochs=50, batch_size=256)

    scores = model.evaluate(test_x, test_y, verbose=0)
    print("Accuracy: %.2f%%" % (scores[1] * 100))

    model.save("l7_50.h5")

    draw(H.history["loss"], "Training loss", ylabel="loss")
    draw(H.history["accuracy"], "Training accuracy", ylabel="accuracy")
    draw(H.history["val_loss"], "Validate loss", ylabel="loss")
    draw(H.history["val_accuracy"], "Validate accuracy", ylabel="accuracy")



if __name__ == '__main__':
    main()
