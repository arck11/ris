from typing import List

import numpy as np
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.datasets import boston_housing
import matplotlib.colors as mclr
import matplotlib.pyplot as plt


def normilize(train_data, test_data):
    mean = train_data.mean(axis=0)
    train_data -= mean
    std = train_data.std(axis=0)
    train_data /= std

    test_data -= mean
    test_data /= std

    return train_data, test_data


def build_model(train_data):
    model = Sequential()
    model.add(Dense(64, activation='relu', input_shape=(train_data.shape[1],)))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(1))
    model.compile(optimizer='rmsprop', loss='mse', metrics=['mae'])
    return model


def draw_mae(maes: List[List]):
    # Построение графика отклонения

    plt.clf()
    for mae in maes:
        epochs = range(1, len(mae))
        plt.plot(epochs, mae[1:], 'b', label='Traning mae')
    plt.title('Training mae')
    plt.xlabel('Epochs')
    plt.ylabel('Mae')
    plt.legend()

    plt.show()


def draw_loss(losses: List[List]):
    # Построение графика ошибки

    plt.clf()
    for loss in losses:
        epochs = range(1, len(loss))
        plt.plot(epochs, loss[1:], 'b', label='Traning loss')
    plt.title('Training loss')
    plt.xlabel('Epochs')
    plt.ylabel('loss')
    plt.legend()

    plt.show()


(train_data, train_targets), (test_data, test_targets) = boston_housing.load_data()

print(train_data)

print(train_data.shape)
print(test_data.shape)

print(test_targets)

train_data, test_data = normilize(train_data, test_data)

k = 5
num_val_samples = len(train_data) // k
num_epochs = 300
all_scores = []
loss_lst = []
maes_lst = []

for i in range(k):
    print('processing fold #', i)
    val_data = train_data[i * num_val_samples: (i + 1) * num_val_samples]
    val_targets = train_targets[i * num_val_samples: (i + 1) * num_val_samples]
    partial_train_data = np.concatenate(
        [
            train_data[:i * num_val_samples],
            train_data[(i + 1) * num_val_samples:]
        ],
        axis=0
    )
    partial_train_targets = np.concatenate(
        [
            train_targets[:i * num_val_samples],
            train_targets[(i + 1) * num_val_samples:]
        ],
        axis=0
    )
    model = build_model(train_data)

    H = model.fit(partial_train_data, partial_train_targets, epochs=num_epochs, batch_size=1, verbose=0)
    val_mse, val_mae = model.evaluate(val_data, val_targets, verbose=0)
    all_scores.append(val_mae)
    loss_lst.append(H.history["loss"])
    maes_lst.append(H.history["mae"])

draw_mae(maes_lst)
draw_loss(loss_lst)

print(np.mean(all_scores))
