import time
from typing import List

import keras
from keras.datasets import cifar10
from keras.models import Model
from keras.layers import Input, Convolution2D, MaxPooling2D, Dense, Dropout, Flatten
from keras.utils import np_utils
import numpy as np

import matplotlib.pyplot as plt


def setup():
    batch_size = 32  # in each iteration, we consider 32 training examples at once
    num_epochs = 200  # we iterate 200 times over the entire training set
    kernel_size = 3  # we will use 3x3 kernels throughout
    pool_size = 2  # we will use 2x2 pooling throughout
    conv_depth_1 = 32  # we will initially have 32 kernels per conv. layer...
    conv_depth_2 = 64  # ...switching to 64 after the first pooling layer
    drop_prob_1 = 0.25  # dropout after pooling with probability 0.25
    drop_prob_2 = 0.5  # dropout in the dense layer with probability 0.5
    hidden_size = 512  # the dense layer will have 512 neurons

    return locals()


def get_data():
    (X_train, y_train), (X_test, y_test) = cifar10.load_data()  # fetch CIFAR-10 data

    num_train, depth, height, width = X_train.shape  # there are 50000 training examples in CIFAR-10
    num_test = X_test.shape[0]  # there are 10000 test examples in CIFAR-10
    num_classes = np.unique(y_train).shape[0]  # there are 10 image classes

    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')

    X_train /= np.max(X_train)  # Normalise data to [0, 1] range
    X_test /= np.max(X_train)  # Normalise data to [0, 1] range

    Y_train = np_utils.to_categorical(y_train, num_classes)  # One-hot encode the labels
    Y_test = np_utils.to_categorical(y_test, num_classes)  # One-hot encode the labels


batch_size = 32  # in each iteration, we consider 32 training examples at once
num_epochs = 4  # we iterate 200 times over the entire training set
kernel_size = 3  # we will use 3x3 kernels throughout
pool_size = 2  # we will use 2x2 pooling throughout
conv_depth_1 = 32  # we will initially have 32 kernels per conv. layer...
conv_depth_2 = 64  # ...switching to 64 after the first pooling layer
drop_prob_1 = 0.25  # dropout after pooling with probability 0.25
drop_prob_2 = 0.5  # dropout in the dense layer with probability 0.5
hidden_size = 512  # the dense layer will have 512 neurons


def get_model():
    (X_train, y_train), (X_test, y_test) = cifar10.load_data()  # fetch CIFAR-10 data

    num_train, depth, height, width = X_train.shape  # there are 50000 training examples in CIFAR-10
    num_test = X_test.shape[0]  # there are 10000 test examples in CIFAR-10
    num_classes = np.unique(y_train).shape[0]  # there are 10 image classes

    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')

    X_train /= np.max(X_train)  # Normalise data to [0, 1] range
    X_test /= np.max(X_train)  # Normalise data to [0, 1] range

    Y_train = np_utils.to_categorical(y_train, num_classes)  # One-hot encode the labels
    Y_test = np_utils.to_categorical(y_test, num_classes)  # One-hot encode the labels

    inp = Input(shape=(depth, height, width))  # N.B. depth goes first in Keras

    # Conv [32] -> Conv [32] -> Pool (with dropout on the pooling layer)
    conv_1 = Convolution2D(conv_depth_1, kernel_size, kernel_size, border_mode='same', activation='relu')(inp)
    conv_2 = Convolution2D(conv_depth_1, kernel_size, kernel_size, border_mode='same', activation='relu')(conv_1)
    pool_1 = MaxPooling2D(pool_size=(pool_size, pool_size))(conv_2)
    drop_1 = Dropout(drop_prob_1)(pool_1)

    # Conv [64] -> Conv [64] -> Pool (with dropout on the pooling layer)
    conv_3 = Convolution2D(conv_depth_2, kernel_size, kernel_size, border_mode='same', activation='relu')(drop_1)
    conv_4 = Convolution2D(conv_depth_2, kernel_size, kernel_size, border_mode='same', activation='relu')(conv_3)
    pool_2 = MaxPooling2D(pool_size=(pool_size, pool_size))(conv_4)
    drop_2 = Dropout(drop_prob_1)(pool_2)

    # Now flatten to 1D, apply Dense -> ReLU (with dropout) -> softmax
    flat = Flatten()(drop_2)
    hidden = Dense(hidden_size << 1, activation='relu')(flat)
    hidden2 = Dense(hidden_size << 1, activation='relu')(hidden)

    drop_3 = Dropout(drop_prob_2)(hidden2)
    out = Dense(num_classes, activation='softmax')(drop_3)

    model = Model(input=inp, output=out)  # To define a model, just specify its input and output layers

    model.compile(loss='categorical_crossentropy',  # using the cross-entropy loss function
                  optimizer='adam',  # using the Adam optimiser
                  metrics=['accuracy'])  # reporting the accuracy

    return model, X_train, Y_train, X_test, Y_test


def draw_accuracy(data: List[List]):
    # Построение графика отклонения

    plt.clf()

    epochs = range(1, len(data) + 1)
    plt.plot(epochs, data, 'b', label='Traning accuracy')
    plt.title('Training accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Mae')
    plt.legend()

    plt.show()


def draw_loss(data: List[List]):
    # Построение графика ошибки

    plt.clf()

    epochs = range(1, len(data) + 1)
    plt.plot(epochs, data, 'b', label='Traning loss')
    plt.title('Training loss')
    plt.xlabel('Epochs')
    plt.ylabel('loss')
    plt.legend()

    plt.show()


def draw(data: List[List], label: str, *, xlabel: str = "Epochs", ylabel: str = None):
    # Построение графика

    plt.clf()

    epochs = range(1, len(data) + 1)
    plt.plot(epochs, data, 'b', label=label)
    plt.title(label)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()

    plt.show()


def train(model_name: str = None):
    if not model_name:
        model_name = "l5_model_{}.h5".format(time.time().replace('.', '_'))

    model, X_train, Y_train, X_test, Y_test = get_model()
    H = model.fit(X_train, Y_train,  # Train the model using the training set...
                  batch_size=batch_size, epochs=num_epochs,
                  verbose=1, validation_data=(X_test, Y_test))
    model.evaluate(X_test, Y_test, verbose=1)  # Evaluate the trained model on the test set!
    model.summary()
    model.save(model_name)
    draw(H.history["loss"], "Training loss", ylabel="loss")
    draw(H.history["accuracy"], "Training accuracy", ylabel="accuracy")
    draw(H.history["val_loss"], "Validate loss", ylabel="loss")
    draw(H.history["val_accuracy"], "Validate accuracy", ylabel="accuracy")


def evaluate(model_name: str = None):
    model, X_train, Y_train, X_test, Y_test = get_model()

    new_model = keras.models.load_model(model_name)

    # Покажем архитектуру модели
    new_model.summary()

    test_loss, test_acc = new_model.evaluate(X_test, Y_test, verbose=1)  # Evaluate the trained model on the test set!
    print(test_loss, test_acc)


if __name__ == '__main__':
    file = "l5_model_10_113.h5"
    train(file)
    evaluate(file)
