from typing import List

import tensorflow as tf
from keras.utils import to_categorical
from tensorflow.keras.layers import Dense, Activation, Flatten
from tensorflow.keras.models import Sequential

import matplotlib.pyplot as plt


def build_model():
    model = Sequential()
    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dense(28, activation='relu'))
    model.add(Dense(28, activation='relu'))
    model.add(Dense(28, activation='relu'))
    model.add(Dense(28, activation='relu'))
    model.add(Dense(28, activation='relu'))
    model.add(Dense(10, activation='softmax'))

    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

    return model


def draw_accuracy(data: List[List]):
    # Построение графика отклонения

    plt.clf()

    epochs = range(1, len(data) + 1)
    plt.plot(epochs, data, 'b', label='Traning accuracy')
    plt.title('Training accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Mae')
    plt.legend()

    plt.show()


def draw_loss(data: List[List]):
    # Построение графика ошибки

    plt.clf()

    epochs = range(1, len(data) + 1)
    plt.plot(epochs, data, 'b', label='Traning loss')
    plt.title('Training loss')
    plt.xlabel('Epochs')
    plt.ylabel('loss')
    plt.legend()

    plt.show()


def main():
    mnist = tf.keras.datasets.mnist
    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()

    train_images = train_images / 255.0
    test_images = test_images / 255.0

    train_labels = to_categorical(train_labels)
    test_labels = to_categorical(test_labels)

    model = build_model()

    try:
        H = model.fit(train_images, train_labels, epochs=50, batch_size=128)
    finally:
        draw_loss(H.history["loss"])
        draw_accuracy(H.history["accuracy"])

        test_loss, test_acc = model.evaluate(test_images, test_labels)
        print('test_loss:', test_loss)
        print('test_acc:', test_acc)


if __name__ == '__main__':
    main()
